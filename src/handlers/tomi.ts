import sharp from "sharp";
import { IHandler } from "../index";

export const file = Bun.file('./public/base-images/tomi.png');

export const args: IHandler["args"] = {
  leftString: {
    required: true,
    type: 'text',
    topOffset: 36,
    leftOffset: 146,
    maxHeight: 84,
    maxWidth: 95
  },
  rightString: {
    required: true,
    type: 'text',
    topOffset: 34,
    leftOffset: 418,
    maxHeight: 19,
    maxWidth: 60
  }
}

export default async function handler({ leftString, rightString }: Record<string, string>): Promise<Blob> {
  const sourceImage = sharp(await file.arrayBuffer());

  const toComposite: sharp.OverlayOptions[] = [];

  toComposite.push(
    {
      input: {
        text: {
          text: leftString,
          align: 'center',
          height: args.leftString.maxHeight,
          width: args.leftString.maxWidth,
        },
      },
      top: args.leftString.topOffset,
      left: args.leftString.leftOffset,
      blend: 'difference' // TODO: Outline text slightly
    })

  toComposite.push(
    {
      input: {
        text: {
          text: rightString,
          align: 'center',
          height: args.rightString.maxHeight,
          width: args.rightString.maxWidth! - 2,
        },
      },
      top: args.rightString.topOffset,
      left: args.rightString.leftOffset,
      blend: 'difference' // TODO: Outline text slightly
    });
  
  sourceImage.composite(toComposite);

  return new Blob([await sourceImage.png().toBuffer()], { type: 'image/png' });
}