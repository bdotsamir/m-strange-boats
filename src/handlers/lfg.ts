import sharp from "sharp";
import { IHandler } from "../index";

const file = Bun.file('./public/base-images/lfg.jpeg');

export const args: IHandler["args"] = {
  topString: {
    type: 'text',
    required: false,
    topOffset: 18,
    leftOffset: 18,
    maxWidth: 650,
    maxHeight: 112
  },
  bottomString: {
    type: 'text',
    required: false,
    topOffset: 549,
    leftOffset: 18,
    maxWidth: 650,
    maxHeight: 112
  }
}

export default async function handler({ topString, bottomString }: Record<string, string>): Promise<Blob> {
  const sourceImage = sharp(await file.arrayBuffer());

  const toComposite: sharp.OverlayOptions[] = []

  if (topString) {
    toComposite.push(
      {
        input: {
          create: {
            width: args.topString.maxWidth!,
            height: args.topString.maxHeight!,
            channels: 3,
            background: { r: 255, g: 255, b: 255 }
          }
        },
        top: args.topString.topOffset,
        left: args.topString.leftOffset,
      },
      {
        input: {
          text: {
            text: topString.toUpperCase(),
            align: 'center',
            height: args.topString.maxHeight,
            width: args.topString.maxWidth,
          },
        },
        top: args.topString.topOffset,
        left: args.topString.leftOffset,
        blend: 'difference' // TODO: Outline text slightly
      })
  }

  if (bottomString) {
    toComposite.push(
      {
        input: {
          create: {
            width: args.bottomString.maxWidth!,
            height: args.bottomString.maxHeight!,
            channels: 3,
            background: { r: 255, g: 255, b: 255 }
          }
        },
        top: args.bottomString.topOffset,
        left: args.bottomString.leftOffset,
      },
      {
        input: {
          text: {
            text: bottomString.toUpperCase(),
            align: 'center',
            height: args.bottomString.maxHeight,
            width: args.bottomString.maxWidth,
          },
        },
        top: args.bottomString.topOffset,
        left: args.bottomString.leftOffset,
        blend: 'difference' // TODO: Outline text slightly
      })
  }

  sourceImage.composite(toComposite);

  return new Blob([await sourceImage.png().toBuffer()], { type: 'image/png' });
}