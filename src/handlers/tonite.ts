import sharp from "sharp";
import { IHandler } from "../index";

export const file = Bun.file('./public/base-images/tonite.jpg');

export const args: IHandler["args"] = {
  string: {
    required: true,
    type: 'text',
    offsets: [
      { leftOffset: 51, topOffset: 76 },
      { leftOffset: 53, topOffset: 223 },
      { leftOffset: 54, topOffset: 348 }
    ],
    dimensions: [
      { maxWidth: 95, maxHeight: 31 },
      { maxWidth: 91, maxHeight: 26 },
      { maxWidth: 89, maxHeight: 22 }
    ]
  }
}

export default async function handler({ string }: Record<string, string>): Promise<Blob> {
  const sourceImage = sharp(await file.arrayBuffer());

  const toComposite: sharp.OverlayOptions[] = [];

  for (let i = 0; i < args.string.offsets!.length; i++) {
    toComposite.push(
      {
        input: {
          create: {
            width: args.string.dimensions![i].maxWidth,
            height: args.string.dimensions![i].maxHeight,
            channels: 3,
            background: { r: 255, g: 255, b: 255 }
          }
        },
        top: args.string.offsets![i].topOffset,
        left: args.string.offsets![i].leftOffset,
      },
      {
        input: {
          text: {
            text: string.toUpperCase(),
            align: 'center',
            height: args.string.dimensions![i].maxHeight,
            width: args.string.dimensions![i].maxWidth,
          },
        },
        top: args.string.offsets![i].topOffset,
        left: args.string.offsets![i].leftOffset,
        blend: 'difference' // TODO: Outline text slightly
      })
  }

  sourceImage.composite(toComposite);

  return new Blob([await sourceImage.jpeg().toBuffer()], { type: 'image/png' });

}