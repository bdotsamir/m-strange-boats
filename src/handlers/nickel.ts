import sharp from "sharp";
import { IHandler } from "..";

export const file = Bun.file("./public/base-images/nickel.png");

export const args: IHandler["args"] = {
  string: {
    type: 'text',
    required: true,
    leftOffset: 630,
    topOffset: 123,
    maxWidth: 554,
    maxHeight: 314
  }
}

export default async function handler({ string }: Record<string, string>): Promise<Blob> {
  const sourceImage = sharp(await file.arrayBuffer());

  const toComposite: sharp.OverlayOptions[] = [
    {
      input: {
        create: {
          width: args.string.maxWidth!,
          height: args.string.maxHeight!,
          channels: 3,
          background: { r: 255, g: 255, b: 255 }
        }
      },
      top: args.string.topOffset,
      left: args.string.leftOffset,
    },
    {
      input: {
        text: {
          text: string.toUpperCase(),
          align: 'center',
          height: args.string.maxHeight,
          width: args.string.maxWidth,
        },
      },
      top: args.string.topOffset,
      left: args.string.leftOffset,
      blend: 'difference' // TODO: Outline text slightly
    }
  ];

  sourceImage.composite(toComposite);

  return new Blob([await sourceImage.png().toBuffer()], { type: 'image/png' });
}