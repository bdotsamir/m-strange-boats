import sharp from "sharp";
import { IHandler } from "..";

const file = Bun.file('./public/base-images/lying.png')

export const args: IHandler["args"] = {
  pronoun: {
    type: 'text',
    required: false,
    leftOffset: 66,
    topOffset: 7,
    maxWidth: 83,
    maxHeight: 48
  },
  str1: {
    type: 'text',
    required: true,
    leftOffset: 166,
    topOffset: 450,
    maxWidth: 81,
    maxHeight: 45
  },
  str2: {
    type: 'text',
    required: true,
    leftOffset: 342,
    topOffset: 450,
    maxWidth: 99,
    maxHeight: 46
  },
  imageURL: {
    type: 'image',
    required: false,
    leftOffset: 10,
    topOffset: 304,
    maxWidth: 141,
    maxHeight: 139
  }
}

export default async function handler({ pronoun, str1, str2, imageURL }: Record<string, string>): Promise<Blob> {
  const sourceImage = sharp(await file.arrayBuffer());

  const toComposite: sharp.OverlayOptions[] = [
    {
      input: {
        create: {
          width: args.pronoun.maxWidth!,
          height: args.pronoun.maxHeight!,
          channels: 3,
          background: { r: 255, g: 255, b: 255 }
        }
      },
      top: args.pronoun.topOffset,
      left: args.pronoun.leftOffset,
    },
    {
      input: {
        text: {
          text: pronoun.toUpperCase(),
          align: 'center',
          height: args.pronoun.maxHeight,
          width: args.pronoun.maxWidth,
        },
      },
      top: args.pronoun.topOffset,
      left: args.pronoun.leftOffset,
      blend: 'difference' // TODO: Outline text slightly
    },
    {
      input: {
        create: {
          width: args.str1.maxWidth!,
          height: args.str1.maxHeight!,
          channels: 3,
          background: { r: 255, g: 255, b: 255 }
        }
      },
      top: args.str1.topOffset,
      left: args.str1.leftOffset,
    },
    {
      input: {
        text: {
          text: str1.toUpperCase(),
          align: 'center',
          height: args.str1.maxHeight,
          width: args.str1.maxWidth,
        },
      },
      top: args.str1.topOffset,
      left: args.str1.leftOffset,
      blend: 'difference' // TODO: Outline text slightly
    },
    {
      input: {
        create: {
          width: args.str2.maxWidth!,
          height: args.str2.maxHeight!,
          channels: 3,
          background: { r: 255, g: 255, b: 255 }
        }
      },
      top: args.str2.topOffset,
      left: args.str2.leftOffset,
    },
    {
      input: {
        text: {
          text: str2.toUpperCase(),
          align: 'center',
          height: args.str2.maxHeight,
          width: args.str2.maxWidth,
        },
      },
      top: args.str2.topOffset,
      left: args.str2.leftOffset,
      blend: 'difference' // TODO: Outline text slightly
    }
  ]

  if (imageURL) {
    const fetchedImage = await fetch(imageURL);
    const resizedImage = await sharp(await fetchedImage.arrayBuffer())
      .resize(args.imageURL.maxWidth, args.imageURL.maxHeight, { fit: "inside" })
      .png().toBuffer();
    
    toComposite.push({
      input: resizedImage,
      top: args.imageURL.topOffset,
      left: args.imageURL.leftOffset
    })
  }

  sourceImage.composite(toComposite);

  return new Blob([await sourceImage.png().toBuffer()], { type: 'image/png' });
}