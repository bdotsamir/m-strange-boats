import sharp from "sharp";
import { IHandler } from "../index";

export const args: IHandler["args"] = {
  text: {
    type: 'text',
    required: true
  }
}

export default async function handler({ text }: Record<string, string>): Promise<Blob> {
  const textImage = sharp({
    create: {
      width: 500,
      height: 100,
      channels: 3,
      background: { r: 255, g: 255, b: 255 }
    }
  });

  textImage.composite([{
    input: {
      text: {
        text: `<span foreground="black">${text.toUpperCase()}</span>`,
        // font: 'Impact',
        height: 25,
        width: 325,
        rgba: true
      }
    }
  }])

  return new Blob([await textImage.png().toBuffer()], { type: 'image/png' });
}