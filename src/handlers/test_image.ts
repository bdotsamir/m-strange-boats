import sharp from "sharp";
import { IArg, IHandler } from "../index";

export const file = Bun.file('./public/base-images/test_image.png');

export const args: IHandler["args"] = {
  topString: {
    type: 'text',
    required: true,
    topOffset: 50,
    leftOffset: 50,
    maxWidth: 400,
    maxHeight: 100
  },
  imageURL: {
    type: 'image',
    required: false,
    topOffset: 165,
    leftOffset: 165,
    maxWidth: 170,
    maxHeight: 170
  }
}

export default async function handler({ topString, imageURL }: Record<string, string>): Promise<Blob> {
  let sourceImage = sharp(await file.arrayBuffer());

  const toComposite: sharp.OverlayOptions[] = []

  if (topString) {
    toComposite.push({
      input: {
        text: {
          text: topString.toUpperCase(),
          align: 'center',
          height: args.topString.maxHeight,
          width: args.topString.maxWidth,
        },
      },
      top: args.topString.topOffset,
      left: args.topString.leftOffset,
      blend: 'difference' // TODO: Outline text slightly
    })
  }

  if (imageURL) {
    const fetchedImage = await fetch(imageURL);
    const resizedImage = await sharp(await fetchedImage.arrayBuffer())
      .resize(args.imageURL.maxWidth, args.imageURL.maxHeight, { fit: "contain" })
      .png().toBuffer();

    toComposite.push({
      input: resizedImage,
      top: args.imageURL.topOffset,
      left: args.imageURL.leftOffset
    });
  }

  sourceImage.composite(toComposite);

  return new Blob([await sourceImage.png().toBuffer()], { type: 'image/png' });
}