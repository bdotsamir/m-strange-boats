import sharp from "sharp";
import { IHandler } from "../index";

export const file = Bun.file('./public/base-images/pillbug.png');

export const args: IHandler["args"] = {
  imageURL: {
    type: 'image',
    required: true,
    topOffset: 15,
    leftOffset: 301,
    maxWidth: 488,
    maxHeight: 286
  }
}

export default async function handler({ imageURL }: Record<string, string>): Promise<Blob> {
  const sourceImage = sharp(await file.arrayBuffer());

  const fetchedImage = await fetch(imageURL);
  const resizedImage = await sharp(await fetchedImage.arrayBuffer())
    .resize(args.imageURL.maxWidth, args.imageURL.maxHeight, { fit: "inside" })
    .png().toBuffer();

  sourceImage.composite([
    {
      input: resizedImage,
      top: args.imageURL.topOffset,
      left: args.imageURL.leftOffset
    }
  ]);

  return new Blob([await sourceImage.png().toBuffer()], { type: 'image/png' });
}