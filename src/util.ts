import { join } from "path";
import sharp from "sharp";

export function calculateTextDPI(text: string, maxWidth: number, maxHeight: number): number {
  const textLength = text.length;
  const textArea = maxWidth * maxHeight;
  const textDensity = textLength / textArea;
  const textDPI = textDensity * 1000000;

  return textDPI;
}

export async function outlineText(text: string, maxWidth: number, maxHeight: number) {
  // 1. Create image from text
  // 2. Split image by each character (interval every character width)
  // 3. For each new image, duplicate, invert.
  // 4. Resize duplicate image to be just a hair larger than original character image.
  // 5. Composite original split image on top of duplicate image.
  // 6. Composite the above composite image onto new canvas

  throw new Error("Not implemented");

  const originalTextImage = sharp({
    create: {
      width: maxWidth,
      height: maxHeight,
      channels: 3,
      background: { r: 255, g: 255, b: 255 }
    }
  });

  const fontFilePath = join(import.meta.dir, '../public/fonts/unicode.impact.ttf');
  console.log(fontFilePath);

  originalTextImage.composite([{
    input: {
      text: {
        text: `<span foreground="black">${text.toUpperCase()}</span>`,
        // font: 'Impact',
        fontfile: fontFilePath,
        height: maxHeight,
        width: maxWidth,
        rgba: true
      }
    }
  }])

  Bun.write('originalTextImage.png', await originalTextImage.png().toBuffer());
}

export async function generateErrorImage(text: string): Promise<Blob> {
  const errorImage = sharp({
    create: {
      width: 500,
      height: 100,
      channels: 3,
      background: { r: 255, g: 255, b: 255 }
    }
  });

  errorImage.composite([{
    input: {
      text: {
        text: `<span foreground="red"><b>Error:</b></span> <span foreground="black">${text}</span>`,
        // font: 'Impact',
        height: 30,
        width: 450,
        rgba: true
      }
    }
  }])

  return new Blob([await errorImage.png().toBuffer()], { type: 'image/png' });
}