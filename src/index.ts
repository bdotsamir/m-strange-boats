import { Elysia, t } from "elysia";
import { readdir } from "node:fs/promises";
import { join } from "node:path";
import { BunFile } from "bun";
import { generateErrorImage } from "./util";

const app = new Elysia();

export interface IArg {
  type: "text" | "image"
  required: boolean,
  topOffset?: number,
  leftOffset?: number,
  offsets?: { topOffset: number, leftOffset: number }[],
  maxWidth?: number,
  maxHeight?: number,
  dimensions?: { maxHeight: number, maxWidth: number }[]
}

export interface IHandler {
  file: BunFile,
  args: {
    [x: string]: IArg
  },
  default: (args: Record<string, any>) => Promise<Blob>
}

const handlers = new Map<string, IHandler>();

const handlerFiles = await readdir(join(import.meta.dir, './handlers'));
for (const handlerFile of handlerFiles) {
  import(`./handlers/${handlerFile}`)
    .then((imported: IHandler) => {
      const handlerName = handlerFile.split('.')[0];
      handlers.set(handlerName, imported);
      console.log(`Imported handler ${handlerName}`);
    })
    .catch(error => {
      console.error(`Error importing file ${handlerFile}:\n`, error);
    })
}

app.get('/:file', ({ params: { file } }) => {
  const bunFile = Bun.file(`./public/${file}`);
  if (bunFile.size === 0) {
    return { "error": "404 Not Found" };
  }

  return bunFile;
});

app.derive(({ query }) => {
  // Replace all instances of _ with a space in the query strings
  const cleanQuery = { ...query };
  delete cleanQuery.src; // remove src from the cloned query object.
  Object.entries(query).forEach(([key, value]) => {
    if (value && key !== 'imageURL' && key !== 'src') {
      cleanQuery[key] = value.replaceAll('_', ' ');
    }
  });

  return {
    src: query.src,
    cleanQuery
  }
}).get('/generate', async ({ cleanQuery, src }) => {
  if (!src) {
    return await generateErrorImage('Missing required argument: src');
  }

  const handler = handlers.get(src);
  if (!handler) {
    return await generateErrorImage(`Unknown source: ${src}. Select from: ${[...handlers.keys()].join(', ')}`);
  }

  const args = handler.args;
  const missingArgs = Object.entries(args).filter(([argName, argValue]) =>
    argValue.required && !cleanQuery[argName]);
  if (missingArgs.length > 0) {
    return await generateErrorImage(`Missing required arguments: ${missingArgs.map(arg => arg[0]).join(', ')}`);
  }

  const unknownArgs = Object.keys(cleanQuery).filter(argName => !args[argName]);
  if (unknownArgs.length > 0) {
    return await generateErrorImage(`Unknown arguments: ${unknownArgs.join(', ')}. Select from: ${Object.keys(args).join(', ')}`);
  }

  const generated = await handler.default(cleanQuery);
  // Bun.write('./public/generated.png', generated);
  return generated;
});

app.get('/sources', () => {
  return { sources: [...handlers.keys()] };
})

app.listen(13371, () => console.log(
  `🦊 Elysia m.strange.boats is running at ${app.server?.hostname}:${app.server?.port}`
));
