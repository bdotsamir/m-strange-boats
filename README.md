# m.strange.boats

Quick and easy image generation server. Many-a-time have I thought "hm this would be a good meme" but I don't want to open up photoshop or gimp and do it myself. This is the solution.

## Stack:
* [Bun](https://bun.sh) - v1.0.8 seems to segfault on aarch64 chips, v1.0.7 is fine.
* [Sharp](https://sharp.pixelplumbing.com) - Image processing
* Whatever reverse proxy you want. I use [nginx](https://nginx.org).

## Setup
1. Install the required system dependencies above.
2. Create a service file that will run the server.

## Usage
**Base url: `https://your.domain/generate?src=<source_image>&arg1=val1&arg2=val2...`**
* `src` - The source image. Predefined as whatever's in the [public/base-images](/public/base-images/) folder.
* `args` - The arguments to pass into the [handler](/src/handlers) function. Check out the handler file of the image you want to use to see what arguments are available.